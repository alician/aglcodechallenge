
## Prerequisite

1. Visual studio 2017.
2. node.js: http://nodejs.org (make sure npm is also installed)
---

## Run Project

1. Restore Nugget packages
2. Set AGLCodeChallenge as start up project
3. Debug in Chrome/IE
---

