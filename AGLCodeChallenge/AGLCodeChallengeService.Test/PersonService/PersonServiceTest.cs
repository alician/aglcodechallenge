﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using Moq;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Text;
using AGLCodeChallengeService.CustomException;

namespace AGLCodeChallengeService.Test.PersonService
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class PersonServiceTest
    {

        /// <summary>
        /// The mock data
        /// </summary>
        public string MockData;
        /// <summary>
        /// The response message
        /// </summary>
        public HttpResponseMessage _responseMessage;
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonServiceTest"/> class.
        /// </summary>
        public PersonServiceTest()
        {
            MockData = File.ReadAllText("../../Data/mockData.json");
        }
        /// <summary>
        /// Initilises this instance.
        /// </summary>
        [TestInitialize]
        public void Initilise()
        {
            _responseMessage = new HttpResponseMessage();
        }
        /// <summary>
        /// Gets the asynchronous success.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetAsync_Success()
        {
            //Setup
            _responseMessage.Content = new FakeHttpContent(MockData);
            var messageHandler = new FakeHttpMessageHandler(_responseMessage);

            //Test
            var service = new AGLCodeChallengeService.PersonService.PersonService(new HttpClient(messageHandler));
            var data = await service.GetAsync();

            //Assert
            Assert.AreEqual(data.Count, 6);
        }
        /// <summary>
        /// Gets the asynchronous fail.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        [ExpectedException(typeof(PersonServiceException))]
        public async Task GetAsync_Fail()
        {
            //Setup
            _responseMessage.Content = new FakeHttpContent(string.Empty);
            var messageHandler = new FakeHttpMessageHandler(_responseMessage);

            //Test
            var service = new AGLCodeChallengeService.PersonService.PersonService(new HttpClient(messageHandler));
            var data = await service.GetAsync();
            
        }

        #region Helper
        /// <summary>
        /// 
        /// </summary>
        /// <seealso cref="System.Net.Http.HttpMessageHandler" />
        public class FakeHttpMessageHandler : HttpMessageHandler
        {
            /// <summary>
            /// The response
            /// </summary>
            private HttpResponseMessage response;

            /// <summary>
            /// Initializes a new instance of the <see cref="FakeHttpMessageHandler"/> class.
            /// </summary>
            /// <param name="response">The response.</param>
            public FakeHttpMessageHandler(HttpResponseMessage response)
            {
                this.response = response;
            }

            /// <summary>
            /// Send an HTTP request as an asynchronous operation.
            /// </summary>
            /// <param name="request">The HTTP request message to send.</param>
            /// <param name="cancellationToken">The cancellation token to cancel operation.</param>
            /// <returns>
            /// Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation.
            /// </returns>
            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                var responseTask = new TaskCompletionSource<HttpResponseMessage>();
                responseTask.SetResult(response);

                return responseTask.Task;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <seealso cref="System.Net.Http.HttpContent" />
        public class FakeHttpContent : HttpContent
        {
            /// <summary>
            /// Gets or sets the content.
            /// </summary>
            /// <value>
            /// The content.
            /// </value>
            public string Content { get; set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="FakeHttpContent"/> class.
            /// </summary>
            /// <param name="content">The content.</param>
            public FakeHttpContent(string content)
            {
                Content = content;
            }

            /// <summary>
            /// Serialize the HTTP content to a stream as an asynchronous operation.
            /// </summary>
            /// <param name="stream">The target stream.</param>
            /// <param name="context">Information about the transport (channel binding token, for example). This parameter may be null.</param>
            /// <returns>
            /// Returns <see cref="T:System.Threading.Tasks.Task" />.The task object representing the asynchronous operation.
            /// </returns>
            protected async override Task SerializeToStreamAsync(Stream stream, TransportContext context)
            {
                byte[] byteArray = Encoding.ASCII.GetBytes(Content);
                await stream.WriteAsync(byteArray, 0, Content.Length);
            }

            /// <summary>
            /// Determines whether the HTTP content has a valid length in bytes.
            /// </summary>
            /// <param name="length">The length in bytes of the HTTP content.</param>
            /// <returns>
            /// Returns <see cref="T:System.Boolean" />.true if <paramref name="length" /> is a valid length; otherwise, false.
            /// </returns>
            protected override bool TryComputeLength(out long length)
            {
                length = Content.Length;
                return true;
            }
        }
        #endregion
    }
}
