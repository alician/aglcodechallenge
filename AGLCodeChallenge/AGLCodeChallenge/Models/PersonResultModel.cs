﻿using AGLCodeChallengeCommon.Person;
using System.Collections.Generic;
using System.Linq;

namespace AGLCodeChallenge.Models
{
    public class PersonResultModel
    {
        public ILookup<string, Person> Data { get; set; }
    }
}