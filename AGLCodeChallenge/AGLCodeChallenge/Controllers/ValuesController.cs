﻿using AGLCodeChallenge.Models;
using AGLCodeChallengeCommon;
using AGLCodeChallengeService.PersonService;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AGLCodeChallenge.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ValuesController : ApiController
    {
        //TODO: move to a seperate class for cleaner code
        private readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// The person service
        /// </summary>
        private IPersonService _personService;
        /// <summary>
        /// Initializes a new instance of the <see cref="ValuesController"/> class.
        /// </summary>
        /// <param name="personService">The person service.</param>
        public ValuesController(IPersonService personService)
        {
            _personService = personService;
        }
        // GET api/values
        /// <summary>
        /// Gets data asynchronously.
        /// </summary>
        /// <param name="pet">The pet to choose</param>
        /// <returns>List of people, group by gender, sort by name</returns>
        public async Task<HttpResponseMessage> GetAsync(string pet)
        {
            try
            {
                Log.Info($"GetAsync is called with pet:{pet}");

                //bad input
                if (string.IsNullOrWhiteSpace(pet))
                    return Request.CreateResponse(HttpStatusCode.BadRequest);

                //Get raw data from json api
                var allValues = await _personService.GetAsync(AGLConstants.DefaultUri);
                if (allValues == null || allValues.Count == 0)
                    return Request.CreateResponse(HttpStatusCode.NoContent);

                //Sort data
                var filterValues = allValues
                    .Where(p => p?.Pets != null && p.Pets.Any(pt =>
                            string.Equals(pet, pt.Type, StringComparison.OrdinalIgnoreCase)))
                    .OrderBy(person => person.Name)
                    .ToLookup(pers => pers.Gender);

                //return data
                Log.Debug($"GetAsync return {filterValues.Count} results");
                return Request.CreateResponse(new PersonResultModel()
                { Data = filterValues.Count > 0 ? filterValues : null });
            }
            catch (Exception e)
            {
                Log.Error($"Error thrown in GetAsync, ex: {e.ToString()}");
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError);
        }

        /// <summary>
        /// Posts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        /// <summary>
        /// Puts the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(int id)
        {
        }
    }
}
