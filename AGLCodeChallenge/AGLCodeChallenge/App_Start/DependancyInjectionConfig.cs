﻿using AGLCodeChallengeService.PersonService;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System.Net.Http;
using System.Web.Http;

namespace AGLCodeChallenge.App_Start
{
    public class DependancyInjectionConfig
    {
        public static void RegisterDependancyInjection()
        {
            // Get global configuration & container builder
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;

            #region default injection
            // Register your MVC controllers.
            builder.RegisterControllers(typeof(WebApiApplication).Assembly);

            // Register your API controllers.
            builder.RegisterApiControllers(typeof(WebApiApplication).Assembly);

            // Register model binders that require DI.
            builder.RegisterModelBinders(typeof(WebApiApplication).Assembly);
            builder.RegisterModelBinderProvider();

            // Register web abstractions like HttpContextBase.
            //builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterModule(new AutofacWebTypesModule());

            // Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // Enable property injection into action filters.
            builder.RegisterFilterProvider();

            // Register Web Abstractions
            builder.RegisterModule<AutofacWebTypesModule>();

            // Register Web API Filter
            builder.RegisterWebApiFilterProvider(config);
            #endregion 

            // Register custom types
            #region Register custom types begin
            builder.RegisterType<PersonService>().As<IPersonService>().SingleInstance();
            builder.RegisterType<HttpClient>().SingleInstance();
            #endregion
            // Set the dependency resolver to be Autofac.
            var container = builder.Build();

            // Set the Dependancy Resolver
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}