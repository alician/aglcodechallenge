﻿(function () {
    "use strict";

    angular
        .module("agl.module")
        .factory("homeFact", ["$resource", homeFact]);

    function homeFact($resource) {
        // default api path
        var path = "/api/values";
        var defaultErrorMessage = "Error!";
        

        //methods
        function getValues(params) {
            return $resource(path)
                .get(params)
                .$promise
                .catch(function (response) {
                });
        }
        return {
            getValues: getValues
        }
    }
})()