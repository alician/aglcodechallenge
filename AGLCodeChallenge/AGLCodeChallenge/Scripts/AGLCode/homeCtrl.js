﻿(function () {
    "use strict";

    angular
        .module("agl.module")
        .controller("homeCtrl", ["$scope", "homeFact", homeCtrl]);

    function homeCtrl($scope, homeFact) {
        
        function reset() {
            $scope.result.data = null;
            homeFact.getValues({ pet: $scope.data.pet })
                .then(function (response) {
                    if (response && response.data) {
                        $scope.result.data = response.data;
                    }
                })
        }

        //Exposed data/methods
        $scope.data = $scope.data || {pet: "Cat"};
        $scope.result = $scope.result || {};
        $scope.reset = reset;
        reset();
    }
})()