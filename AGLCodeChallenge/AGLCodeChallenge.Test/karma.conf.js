// Karma configuration
// Generated on Mon Jun 16 2014 15:04:49 GMT+1000 (AUS Eastern Standard Time)
var webpack = require("webpack");
module.exports = function (config) {
    var scriptsBasePath = "AGLCodeChallenge/Scripts/";
    var viewsBasePath = "AGLCodeChallenge/Views/";
    var viewsPath = viewsBasePath + "**/*.cshtml";
    var testsBasePath = "AGLCodeChallenge.Test/Scripts/";
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: "../",


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ["jasmine"],
        plugins: [
            require("karma-webpack"),
            "karma-jasmine",
            "karma-chrome-launcher",
            "karma-firefox-launcher",
            "karma-ie-launcher",
            "karma-ng-html2js-preprocessor",
            "karma-phantomjs-launcher",
            "karma-phantomjs2-launcher",
            "karma-xml-reporter"
        ],


        // list of files / patterns to load in the browser
        files: [
            scriptsBasePath + "angular.js",
            scriptsBasePath + "angular-resource.js",
            scriptsBasePath + "angular-ui/*.js",
            scriptsBasePath + "angular-mocks.js",
            scriptsBasePath + "AGLCode/*.js",
            // Test helpers and mocks
            testsBasePath + "**/*.js"
        ],


        // list of files to exclude
        exclude: [
            'Scripts/*.min.js',
            scriptsBasePath + "*.min.js",
            scriptsBasePath + "*.min.js.map",
            scriptsBasePath + "angular-ui/*.min.js",
            scriptsBasePath + "angular-ui/*.min.js.map",

        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {

        },


        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'xml'],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,

        //webpack
        preprocessors: {},
        webpack: {
            module: {
                loaders: [{
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: "babel-loader"
                }]
            },
            plugins: [
                new webpack.ProvidePlugin({
                    fetch: "imports?this=>global!exports?global.fetch!whatwg-fetch"
                })
            ]
        },

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true
    });
};
