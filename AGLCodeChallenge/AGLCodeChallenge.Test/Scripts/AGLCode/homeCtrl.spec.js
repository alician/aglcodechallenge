﻿describe("homeCtrl", function () { 
    var $rootScope;
    var $scope;
    var homeFact;
    var $q;
    var mockData = {
        data: { female: [], male: [] }
    };
    //set up base
    beforeEach(module("agl.module"));

    //setup injection
    beforeEach(inject(function ($injector) {
        $rootScope = $injector.get("$rootScope");
        $controller = $injector.get("$controller");
        homeFact = $injector.get("homeFact");
        $q = $injector.get("$q");
        $scope = $rootScope.$new();
        $controller("homeCtrl", {
            $scope: $scope,
            homeFact: homeFact
        });
    }));

    //mock
    beforeEach(function () {
        spyOn(homeFact, "getValues").and.returnValue($q.when(mockData));
    });
    it("should set pet to default value", function () {

        expect($scope.data.pet).toEqual("Cat");
    });
    it("should call homeFact.getValues service", function () {
        $scope.reset();

        expect(homeFact.getValues).toHaveBeenCalled();
    });
    it("should set data to mockData", function () {
        $scope.reset();
        $scope.$apply();
        expect(homeFact.getValues).toHaveBeenCalledWith({ pet: "Cat" });
        //expect($scope.result.data).toEqual(mockData.data);
    });

})