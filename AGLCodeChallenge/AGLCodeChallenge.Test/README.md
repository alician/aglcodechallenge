#How to get up and running with the JavaScript unit tests.

## Prerequisites
- **node.js**: http://nodejs.org (make sure npm is also installed)
- **karma-cli**: in the command line interface, browse to the project folder and run ``npm install karma-cli -g``. This will let you run the ``karma`` command in the command line.
- **test dependencies**: in the command line, browse to the project folder and run ``npm install``. This will install the node packages needed to run the tests.

##Running tests
### Start Karma
In the command line, browse to the project folder and run ``karma start``. Karma will start, run the tests, and then watch for file changes in the background.

## Adding new tests or modifying existing tests
The folder structure of the test project mirrors the folder structure of the JavaScript source files. For example, for a source file at:

    /AGLCodeChallenge/Scripts/AGLCode/homeCtrl.js

The test file is at:

    /AGLCodeChallenge.Test/Scripts/AGLCode/homeCtrl.spec.js


Any new files should be added following this structure. Note that the test file is named with ``.spec.js``.

## Configuring tests

``karma.conf.js`` contains settings for the Karma test runner. Most important parts of this file are the ``files``, and ``browsers`` config options. Here, you set the .js files that should be included when running tests, and what browsers Karma should launch to run the tests in.

#How to get up and running with the C# unit tests.

##Running tests
In visual studio, build solution and run/debug test from test explorer
