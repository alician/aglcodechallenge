﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AGLCodeChallengeCommon.Person;
using AGLCodeChallenge.Controllers;
using AGLCodeChallengeService.PersonService;
using Moq;
using System.Net.Http;
using System.Web.Http;
using AGLCodeChallengeCommon;
using AGLCodeChallenge.Models;

namespace AGLCodeChallenge.Test.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class ValuesControllerTest
    {
        /// <summary>
        /// The person service
        /// </summary>
        private Mock<IPersonService> _personService;
        /// <summary>
        /// Initializes a new instance of the <see cref="ValuesControllerTest"/> class.
        /// </summary>
        public ValuesControllerTest()
        {
            _personService = new Mock<IPersonService>();
        }
        /// <summary>
        /// Tests the get values success asynchronous.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestGetValues_SuccessAsync()
        {
            _personService.Setup(m => m.GetAsync(It.IsAny<string>())).Returns(MockData());

            var ctrl = new ValuesController(_personService.Object);
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();
            var data = await ctrl.GetAsync("Cat");

            ObjectContent objContent = data.Content as ObjectContent;
            PersonResultModel content = objContent.Value as PersonResultModel;

            var k = content.Data["Male"];
            Assert.AreEqual(content.Data.Count, 2);
            Assert.IsTrue(content.Data.Contains("Female"));
            Assert.AreEqual(CountData(content.Data["Male"]), 2);
            Assert.AreEqual(GetFirstDataName(content.Data["Male"]), "Alex");
        }
        /// <summary>
        /// Tests the method1.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TestGetValues_FailAsync()
        {
            _personService.Setup(m => m.GetAsync(It.IsAny<string>())).Returns(Task.FromResult<List<Person>>(null));

            var ctrl = new ValuesController(_personService.Object);
            ctrl.Request = new HttpRequestMessage();
            ctrl.Configuration = new HttpConfiguration();
            var data = await ctrl.GetAsync("Cat");

            Assert.AreEqual(data.StatusCode, System.Net.HttpStatusCode.NoContent);
        }

        #region Helper
        /// <summary>
        /// Counts the data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private int CountData(IEnumerable<Person> data)
        {
            var count = 0;
            foreach (var e in data)
            {
                count++;
            }
            return count;
        }
        /// <summary>
        /// Gets the first name of the data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private string GetFirstDataName(IEnumerable<Person> data)
        {
            var name = string.Empty;
            foreach (var e in data)
            {
                name = e.Name;
                break;
            }
            return name;
        }
        /// <summary>
        /// Mocks the data.
        /// </summary>
        /// <returns></returns>
        private Task<List<Person>> MockData()
        {
            var result = new List<Person>();
            var pet = new List<Pet>();
            pet.Add(new Pet() { Name = "Kiki", Type = "dog" });
            pet.Add(new Pet() { Name = "Mimi", Type = "cat" });
            result.Add(new Person() { Name = "Alicia", Gender = "Female", Pets = pet });
            result.Add(new Person() { Name = "Paul", Gender = "Male", Pets = pet });
            result.Add(new Person() { Name = "Alex", Gender = "Male", Pets = pet });

            var tcs = new TaskCompletionSource<List<Person>>();
            tcs.SetResult(result);
            return tcs.Task;
        }
        #endregion
    }
}
