﻿using System;

namespace AGLCodeChallengeService.CustomException
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class PersonServiceException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonServiceException"/> class.
        /// </summary>
        public PersonServiceException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonServiceException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public PersonServiceException(string message)
            : base($"Exception occurs in PersonService: {message}")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonServiceException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        public PersonServiceException(string message, Exception inner)
            : base($"Exception occurs in PersonService: {message}", inner)
        {
        }
    }
}
