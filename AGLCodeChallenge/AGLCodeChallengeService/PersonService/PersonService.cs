﻿using AGLCodeChallengeCommon;
using AGLCodeChallengeCommon.Person;
using AGLCodeChallengeService.CustomException;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace AGLCodeChallengeService.PersonService
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="AGLCodeChallengeService.PersonService.IPersonService" />
    public class PersonService : IPersonService
    {
        /// <summary>
        /// The HTTP client
        /// </summary>
        public HttpClient _httpClient;
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonService"/> class.
        /// </summary>
        /// <param name="httpClient">The HTTP client.</param>
        public PersonService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        /// <summary>
        /// Gets the asynchronous.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns></returns>
        /// <exception cref="PersonServiceException"></exception>
        public async Task<List<Person>> GetAsync(string uri = AGLConstants.DefaultUri)
        {

            var content = await _httpClient.GetStringAsync(uri);

            if (string.IsNullOrWhiteSpace(content))
                throw new PersonServiceException($"Failed to retrieve data from uri: {uri}");

            return JsonConvert.DeserializeObject<List<Person>>(content);


        }
    }
}
