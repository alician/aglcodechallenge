﻿using AGLCodeChallengeCommon;
using AGLCodeChallengeCommon.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGLCodeChallengeService.PersonService
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPersonService
    {
        /// <summary>
        /// Gets the asynchronous.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns></returns>
        Task<List<Person>> GetAsync(string uri = AGLConstants.DefaultUri);
    }
}
